# Main Page Bio226

### [C1 - Introduction to remote work on a server](C1 - Introduction to remote work on a server/ReadMe.md)
<details>
  <summary>  Details </summary>

- 1 Prepare your computer
   - 1.1 Windows: Use PuTTY
   - 1.2 MacOS: Use the terminal
- 2 Connect remotely to our server
  - 2.1 PuTTY
  - 2.2 MacOS terminal
- 3 Get familiar with the command line interface
- 4 Move files between the server and your PC or Mac

</details>

### [C2 - Raw Data](Software/WorkInProgress.md)
[comment]: <> (C2 - Raw Data/ReadMe.md)

### [C3 - Population Structure](Software/WorkInProgress.md)
[comment]: <> (C3 - Population Structure/ReadMe.md)
### [C4 - Handling NGS Data I](Software/WorkInProgress.md)
[comment]: <> (C4 - NGS1/ReadMe.md)
### [C5 - Handling NGS Data II](C5 -NGS2/readme.md)
[comment]: <> ()
### [C6 - Modelling Species Divergence](Software/WorkInProgress.md)
[comment]: <> (C6 - Species Divergence/ReadMe.md)
### [C7 - Selection and Outlier Scans](Software/WorkInProgress.md)
[comment]: <> (C7 - FST_Outliers/ReadMe.md)
### [C8 - Evolution and Selection on Protein-Coding Sequences](Software/WorkInProgress.md)
[comment]: <> (C8 - Evolution and Selection/ReadMe.md)
