# Introduction to next-generation sequence (NGS) data analysis II - Computer session 
---
[[_TOC_]]




# Data
The data for this computer session is the same as for Computer [Session C4](/C4 - NGS1/ReadMe.md). We will work off the files we produced in that session. In addition, **TAIR10_cds_20101214.fas.gz** contains a gzip-compressed FASTA sequence file with Arabidopsis thaliana CDS. The file **TAIR10_cds_20101214.md5** contains the [checksum(md5)](/Software/MD5_Checksum.md) for the FASTA file.

# Software
In this lab, we will use software to obtain annotation information, and to perform variant calling.
Specifically, we will use [BLAST](/Software/BLAST.md) and [Blast2Go](/Software/Blast2GO.md) , as well as samtools and (included with it) [bcftools]() , and finally [Excel]() . For [samtools]() / [bcftools] , we provide an (older) Windows version as well as a (newer) WSL/Linux version – feel free to use one or the other as per your preferences. 

# 1. Sequence Annotation
Beyond the detection of patterns in natural populations, sequence data allow us to assign putative functions to a gene of interest by looking at the (putative) function of homologous genes. This is done in the process of sequence annotation. It is important to realise that all functions assigned by sequence homology are **putative**; they may be fairly close to the truth, or they may entirely unreliable. Nevertheless, having putative functional information available is usually better than having no information at all; putative gene functions assigned by homology therefore often serve as a useful starting point for figuring out what ecological or evolutionary relevance (or indeed what biochemical function) a gene really has. <br>

We will here assign putative functions to the set of reference transcripts we used in the last computer session (file *selected_genes_A.fas* ). We will do this in two ways: 1) by using the commonly used annotation tool [Blast2GO]() , and) by using the [BLASTN]() command-line tool against a custom database (in our case, Arabidopsis CDS). <br>

The basic tool underlying sequence annotation in this exercise is [BLAST]() , which you may know from its public NCBI interface for BLAST web search: http://blast.ncbi.nlm.nih.gov/ <br>

In principle you could search for homologous sequences using this web-interface, but for a large number of sequences this fast becomes impracticable.


## Sequence annotation using Blast2GO
Start Blast2GO from the Desktop shortcut. This program provides an easy-to-use graphical user
interface (GUI) to allow BLAST searches to a public database, and then extract the Gene Ontology
(GO) annotation of the BLAST hits, and then further identify enzyme commission (EC) numbers and membership in Kyoto Encyclopaedia of Genes and Genomes (KEGG) pathways. Blast2GO is
frequently used for annotation of NGS data sets from non-model organisms. Its main disadvantage is
that it is fairly slow. (Also, annoyingly it requires you to register).

### Registering Blast2GO
Once Blast2GO is open, it will ask for an Activation Key. For this you will need to access the blast2go
webpage: https://www.blast2go.com/blast2go-pro/b2g-register-basic and fill in the form. Please
enter your first and family name and e-mail. At the “Institution” field, please write University of
Zurich and, finally, country: Switzerland. Within a couple of minutes, you should get an e-mail with
the activation key. Please copy it in your Blast2GO window and it will start working.

### Blast step in Blast2GO
Load sequences into Blast2GO via File -> Load -> Load Sequences -> Load Fasta File
(.fasta) and then selecting the selected_genes_A.fas file. (If Blast2GO refuses to open
these sequences, make a copy of the file with .fna suffix first and try again.) In your computer
session folder, make a new directory named blast2go_results .
Now start the BLAST analysis by selecting Blast -> Run . You will have to enter your e-mail
address. You can select the default BLAST type (blastx) and database (nr). For convenience, select the
Save results as ... text option. Start the search, select the directory and select
blast2go_results as the results folder. Blast2GO should now start retrieving BLAST hits from
the database. Once it has retrieved the first hits, go into the blast2go_results folder and have a
look at one of the results files ( .txt ) in e.g. NotePad++ (don’t use ‘regular’ Notepad , because it will
not properly display the linux-style line breaks in the file). Have a look at what type of information is
retrieved.
**Important**: Blast2GO is slow. For this reason, please continue now with the 'Finding
homologues by custom BLAST analysis ’ section and only continue with the next section ( GO
mapping in Blast2GO ) once the current task is complete (may take up to ~ 1 hour) or you/we run
out of patience.

### GO mapping in Blast2GO
Note: If Blast2GO it is still running and it does not look like it is close to finishing, then stop the
analysis and instead load the already saved BLAST project file selected_genes_a.b2g by
selecting File -> Open File (.b2g) and continue with the next step.

Start the GO mapping in Blast2GO by selecting Mapping -> Run Mapping . Do not wait for this
step to finish (may take ~ half an hour), but rather continue with your other tasks. Once the
Blast2GO mapping step is done, you can continue with the next section ( Blast2GO annotation
step ).

### Blast2GO annotation step
Note: Again, if this step is taking excessively long, stop the analysis and instead load the file
selected_genes_a_GO.b2g , again via File -> Open File (.b2g) . This file contains the same
output you should have got anyway.

Now go to Annot -> Run Annotation (using the default values) and when this is done (takes a
few minutes) select Analysis -> Enzyme Code and KEGG -> Load Pathway-Maps From
KEGG (online).

### Making sense of Blast2GO output
Your Blast2GO annotation is essentially complete now, at least for the purpose of this exercise. You
can save your results via File -> Save . Scroll through the Blast2GO GUI and see what
information has been retrieved. There is a sequence description that has been obtained from the
BlastX hit, along with some GO IDs, Enzyme (EC) information and KEGG information.

First have a look at some transcripts and their seq description and GO IDs columns. Then, you
can also select a transcript, right click on it and click “ Show GO Descriptions ”. In the Blast/IPS
Results tab, you should see some information on the GO terms associated with the transcript
sequence. If you would like to have further information on any of the GO terms, feel free to go to
http://www.geneontology.org/ and search for the selected GO term (select “GO term” search).

To understand what the EC numbers tell you, you can search for the respective EC numbers on
https://enzyme.expasy.org/ and http://www.brenda-enzymes.de/ . You see there is a wide range of
biochemical (and other) information available there.

To see in which biochemical pathways the annotated transcripts are putatively involved in, you can
go to the Kegg Maps tab. On the right side, you should see a list of Pathways, along with the number
of sequences annotated in the pathway and enzymes in the pathway. Select one of the pathways and
a pathway map should be shown on the left, with colour highlighting of which enzymes in the
pathways are putatively present in the list of annotated sequences, along with a list of the sequence
IDs. If you want to have further information on KEGG pathways, you can have a look at
http://www.genome.jp/kegg/pathway.html.

# 2. Finding homologues by custom BLAST analysis
It is sometimes useful to check sequences of interest against a specific set of homologous sequences
rather than entire public database. This can be significantly faster than a Blast2GO analysis and –
under the right conditions – make interpretation of the results more straightforward. The sequences
used in this course are from a non-model plant and we will here identify the best matches these
sequences have in Arabidopsis thaliana. This is useful because Arabidopsis is the best annotated
plant genome currently available and by identifying homologues in Arabidopsis we may be able to
take advantages of some of the information and tools available there. Information (including genome
sequence and annotation data) from Arabidopsis are available from The Arabidopsis Information
Resource (TAIR, www.arabidopsis.org).

### Extracting TAIR data
To perform this analysis, we first need to unpack and check the file containing the coding sequences
(CDS) from compiled from version 10 of the Arabidopsis genome (TAIR10 release).

Copy the files TAIR10_cds_20101214.fas.gz and TAIR10_cds_20101214.md5 into your
computer session directory (the same as session C4), and open a command prompt there.

First unzip the file:

      gzip -d TAIR10_cds_20101214.fas.gz

And then verify that it is OK:

      md5sum -c TAIR10_cds_20101214.md5


### Setting up the BLAST database
Like with mapping of reads to a reference, we first need to make a reference database for a BLAST
search. In principle this can be any set of sequences you want to use. The BLAST database is set up
using the tool makeblastdb . It is useful first to have a look at the possible command-line
parameters. We will write them to a file:

*[on newer versions, including our Linux executables]*

      makeblastdb -help > makeblastdb_help.txt

*[on older versions, including our Windows executables]*

      makeblastdb.exe -help > makeblastdb_help.txt

Have a look at this text file.

You can now make a BLAST database like such:

      makeblastdb.exe -in TAIR10_cds_20101214.fas -dbtype nucl -title Tair10Cds -out Tair10Cds

### Performing a local BLASTN search
We will here perform a nucleotide-based BLAST search (BLASTN), but the same approach can also be
used for other types of BLAST searches (e.g. BLASTX or BLASTP). Nucleotide BLAST searches are
performed using the blastn executable.

Again, it is useful to have a look at the options first:

*[on newer versions, including our Linux executables]*

      blastn -help > blastn_help.txt

*[on older versions, including our Windows executables]*

      blastn.exe -help > blastn_help.txt


Have a look at the file. In order to carry out the BLAST search, we need to specify which kind of BLAST
search should be performed ( -task ), the database ( -db ), the input file ( -query ), the output file ( -
out ) and the E-value threshold ( -evalue ). Caution: The default search task of blastn is NOT a
blastn search, but a megablast search! Megablast is useful for highly similar sequences (which we
do not necessarily expect in our case).

We can have a look at both, a normal *blastn* and a *megablast* search against the Arabidopsis CDS.
(Here an E-value of 0.001 = 1E-03 is specified):

__*Megablast*__:

*[on newer versions, including our Linux executables]*

      blastn -task megablast -db Tair10Cds -query selected_genes_A.fas -evalue 0.001 -out megablast_SelGenes2Tair.txt

*[on older versions, including our Windows executables]*

      blastn.exe -task megablast -db Tair10Cds -query selected_genes_A.fas -evalue 0.001 -out megablast_SelGenes2Tair.txt

__*BLASTN:*__

*[on newer versions, including our Linux executables]*

      blastn -task blastn -db Tair10Cds -query selected_genes_A.fas -evalue 0.001 -out blastn_SelGenes2Tair.txt

*[on older versions, including our Windows executables]*

      blastn.exe -task blastn -db Tair10Cds -query selected_genes_A.fas -evalue 0.001 -out blastn_SelGenes2Tair.txt

Have a look at the results. You will see that there are only relatively few hits using the megablast
search mode as compared to blastn . It also becomes clear that, while the output format is human-
readable, it is not suitable for filtering through large amounts of data. Ideally, we would like to have a
different output format. Fortunately, you can customise the output of the blastn executable. Have
a look at the help file again, specifically the -outfmt option – it has quite some flexibility. In
particular, we can use the tabular output ( -outfmt option 6 ) to create a tab-delimited text file. For
example, we can run the following:

*[on newer versions, including our Linux executables]*

      blastn -task blastn -db Tair10Cds -query selected_genes_A.fas -evalue 1E-03 -out blastn_SelGenes2Tair.tab -outfmt "6 qseqid sseqid pident length evalue bitscore qstart qend sstart send qseq sseq"

*[on older versions, including our Windows executables]*

      blastn.exe -task blastn -db Tair10Cds -query selected_genes_A.fas -evalue 1E-03 -out blastn_SelGenes2Tair.tab -outfmt "6 qseqid sseqid pident length evalue bitscore qstart qend sstart send qseq sseq"

The output .tab (= .tsv , tab-delimited text/tab-separated values) file would have columns with the
following contents: query sequence ID ( qseqid ), subject sequence ID ( sseqid ), percentage of
identical matches ( pident ), alignment length ( length ), Expect value ( evalue ), bit score
( bitscore ), start and end of alignment in query ( qstart and qend ), start and end of alignment in
subject ( sstart and send ), and the aligned parts of query and subject sequence ( qseq and sseq ).

You can open this output file in Excel , add a row at the top, and label the rows according to your -
outfmt parameters. It is easy to sort and filter BLAST hits in this format. Have a look at a few of the
best hits in Arabidopsis (e.g. transcript 726 ) and select the value in the sseqid column (the
Arabidopsis gene identifier with the format AT#G######.# . You can easily go to TAIR
(www.arabidopsis.org) and at the top right, search for a gene of this name.

From the TAIR website, you can easily get information about the gene model (intron/exon etc.
model), annotation information, available plant lines, down to publications in which this gene was
discussed.


# 3. Variant calling

Variant calling refers to the detection of sequence variants such as single nucleotide polymorphisms
(SNPs) and insertions/deletions (indels). It is also possible to detect more complex variants (such as
structural variants), but we will not discuss these here. There are different pieces of software that
can be used for variant calling, including samtools (http://www.htslib.org), freebayes
(https://github.com/ekg/freebayes) and the genome analysis toolkit ( GATK ;
http://www.broadinstitute.org/gatk/). There are some situations (e.g. with SNP calling), where GATK
is alleged to perform better than samtools . However, samtools is more straightforward to use and
for the sake of simplicity, we will only perform variant calling with samtools here.

### SamTools
There is different variant calling software available (with different strengths and weaknesses), but
here we will use the SamTools package (which can perform various tasks besides variant calling). It
comes in several executables ( .exe files on Windows, no suffix on Linux), but here we will only use
samtools and bcftools . Running them without any command line parameter will display a short
overview of the possible operation modes, and running [Program] [OperationMode] without
further parameters will display a short summary of the possible parameters. Help is available at
http://www.htslib.org/doc/samtools.html or in the samtools-manual.pdf file (which is just a
printed version of the web page).

## Preparing BAM files for variant calling
Variant calling will be performed on binary compressed .bam rather than the human readable .sam
files. We will first have to convert .sam to .bam , then sort and index the .bam files.

Converting .sam to .bam is done using samtools view , specifying the input ( -S ) and output ( -b )
file formats. You have to redirect the program output to a file. You can use the following command
line for this:

      samtools view -b -S MappedReads.sam > MappedReads.bam

For efficient file access, the data records inside the .bam file have to be sorted by position. The exact way of doing this depends on the version of samtools you are using (i.e. the program has changed over time). You can use a command line like this:


*[on older versions, including our Windows executables]:*

     samtools sort MappedReads.bam MappedReads_sorted 


*[on newer versions, including our Linux executables]:*

$ ./samtools sort MappedReads.bam -o MappedReads_sorted.bam



Now the .bam file needs to be indexed using the samtools index command. Indexing will
generate a .bai index file for a given .bam file. Write down the command line you use:

      samtools index MappedReads_sorted.bam

> *__Note 1__*: With an indexed .bam file, we could use the samtools idxstats command to produce
similar summary statistics as we copied from Tablet into Excel . For instance, the command line
samtools idxstats reads_sorted.bam > reads.tab would create a tab-delimited text file
with the first three columns containing contig name, length, and number of reads.

> *__Note__* 2: During variant calling, false heterozygotes may be inferred around indels. The SamTools
manual suggests that using the samtools calmd command can be used to reduce the calling of
false heterozygotes around indels. Technically, this command calculates the MD tag (a string of
mismatching position for variant calling without reference look-up) and stores it in the BAM file. For
instance, the command line samtools calmd -C 50 -E -r -b MappedReads_sorted.bam
ref.fna > MappedReads_sorted_MD.bam could be used to produce such files for use in variant
calling (they also need be sorted [if not yet done] and indexed before variant calling).


### Preparing the reference sequence for variant calling
Samtools needs the reference sequenced to be indexed (differently than bowtie or bwa ). Indexing is
performed using the samtools faidx command. Command line:

      samtools faidx reference.fas

###  Variant calling with the mpileup command
Variant calling can be performed using the samtools mpileup command, which produces a .bcf
file from one or several .bam files mapped to the same reference. Several samples can either be
merged in a single .bam file and annotated by read group ( @RG ), or where several .bam files are
used, each .bam file will be considered to be a separate sample. We will take the latter approach, so
your command line will look something like this:


      samtools mpileup [options] -f Reference.fas Sample1.bam Sample2.bam .. > Variants.bcf

Here, the option -f ensures that the call will work with the indexed reference sequence we have
produced.

The option -C # (with suggested value -C 50 ) downgrades mapping quality for reads with excessive
mismatches. Option -E improves sensitivity. You can specify the minimum phred base quality for a
base to be considered during SNP calling using the -Q # option. Per default, a maximum of up to 250
reads are considered per SNP position; option -d # allows one to provide another maximum value.

You should specify output option -g to ensure that .bcf is produced and genotype likelihoods are
calculated. It is useful to ensure that the output will contain per-sample read-depth information (VCF
DP tag) or optionally, allele-specific read-depth information ( AD tag). To make samtools write a DP
tag, you can use option -D (in newer versions, including our Linux executable, this is equivalent to -t
DP ). Finally, you will have to redirect the program’s output to a file. Please write down the command
line you use for this step. An example would be this:
*[on older versions, including our Windows executables]* :

      samtools mpileup -C 50 -E -Q 13 -D -g -d 1000 -f Reference.fas sample1.bam sample2.bam sample3.bam > Variants.bcf

*[on newer versions, including our Linux executables]* :

      samtools mpileup -C 50 -E -Q 13 -t DP -g -d 1000 -f Reference.fas sample1.bam sample2.bam sample3.bam > Variants.bcf

For your data, this step should only take a couple of minutes.

### Index and view variant calling output
Now the .bcf file needs to be indexed using the bcftools index command. Indexing will
generate an index file ( .bci or .csi ) for a given .bcf file. The command line to do this is:

      bcftools index variants.bcf

   We now convert the .bcf file into the human-readable (tab-delimited text) .vcf variant call format
file. Exactly how this is done, has changed over time and depends on the version of bcftools you
are using; one would use the bcftools view (old versions) or bcftools call (new versions)
command, while redirecting the program’s output into a file. Since per default variant calling also
stores information on invariant sites which do not interest us at the moment, we should specify the
option -v to only output potential variant sites. In an ecological genomics context, we would
typically also be interested in genotype calls for each site (explicit option -g in old versions). overall, the command line to produce our human-readable VCF file has changed with different bcftools
versions, where newer versions of the software allow a choice of two variant callers: a consensus
caller (same as in old versions, set via -c in new versions) or a (better?) caller that is capable of
handling multiple alleles (not just two) at any specific site (option -m ). In our case, either should be
fine. Our command lines would like this:

*[on older versions, including our Windows executables]* :

      bcftools view -v -g variants.bcf > variants.vcf

*[on newer versions, including our Linux executables; set -m/-c as appropriate]* :

      ./bcftools call –[m|c] -v variants.bcf -o variants.vcf


In the case of our data set, the resulting .vcf file should be small enough for you to open and
examine it in Excel (as a tab-delimited file).

# 4. Downstream exploratory data analysis
Once we have obtained variant site calls, we can treat them like any other polymorphism data we
obtain. We may now filter the data based on SNP/indel quality and transform the data into a file
format readable by our software of choice, either by hand or by use of scripts (or other software). In
our case, the data set is small enough to be manipulated in Excel . , using e.g. the Text-to-
Columns functionality and very simple functions and formulae.

You should now have your .vcf file open in Excel. The top lines (i.e., the header of the file) include
some information on what the tags in the INFO and sample columns (the layout of which is explained
in the FORMAT column) contain.

Add a new work sheet to the data and copy the #CHROM , POS , REF , and ALT columns. They contain
the reference transcript name, the position in this reference at which a variant occurs, the base in
the reference transcript, and any other (alternative) bases observed. Using the text-to-columns
function, split the _ALT_ field at commas (“ , ”) and keep leftmost column (the first alternative base; we
will ignore the rest in this exercise).

Now also copy the INFO columns right of these columns. The INFO column contains a wealth of
information, but we will here only look at the coverage or read depth represented by the DP tag. In
our .vcf file, it will usually (but not necessarily) be the tag in the INFO field. We can use the text-
to-columns function to split it by semicolon (“ ; ”) and equals sign (“ = ”). Make sure that the first
resulting column is always DP , and then keep the next column right of DP (the number ## derived
from the DP=## tag). Label this column DP and delete the rest of the columns derived from the INFO
fields.

Now examine the FORMAT column and check that it is the same for all variant positions (it would
usually be GT:PL:DP:GQ or something very similar). Then, for each of the 4 samples, copy the
sample column (named by the bam files you used during mpileup ) to the right of your data and
extract individual columns from them as follows: First split by colon (“ : ”), making sure the output
8Bio226 C5 (2017)
format of the columns is set to text, and then label columns by sample name and tag ( GT , PL , DP ,
GQ ).

Repeat these data extraction steps for all samples and then reorder them, keeping only the DP for
each sample and then the GT fields for each sample.

You now see how you can extract sets of information you are interested in from a .vcf file. But it is
also easy to see why it is advisable to learn some scripting and/or programming skills if you were to
do this frequently. You could now use these data to produce input files for different analysis
programs.

Here, for illustration purposes, we can now do some basic work with the data. First, since the quality
of SNP calls depends on the read coverage (read depth), we may want to filter the data by the DP
field (use the DP field derived from the INFO column). In the column right of your data, add for the
first line a formula like the following : 

      =IF([Cell-of-DP]>=threshold;1;0) 
or

      =IF([Cell-of-DP]>=threshold,1,0)
      
with the appropriate cell reference and a threshold value of your choice (maybe 10 reads). The cell will be 1
if DP is greater than or equal to the threshold value, and 0 otherwise. Copy this formula to the
remaining cells in the column. Cells with 1 pass our read threshold filter, cells with 0 fail. We can
therefore sort the entire data sheet by this column.

How many variant positions are there in the list? How many of those have as many or more reads
than you chose as a threshold? How does this number change when changing the threshold? Briefly
open the Excel sheet produced in exercise C4, and calculate the total number of bases in the set of
reference sequences (the sum of all lengths). Using this number, what is the proportion of variable
sites covered with sufficient reads out of all reference sequences?

We have seen in session C4 that read coverage is uneven among samples. A more stringent
requirement may therefore be to specify a minimum read coverage not across all samples, but for
each sample. In a new column, calculate the minimum read coverage in any of the samples (i.e. DP
from the sample columns) for each variant position by =MIN() . Now make a similar =IF() –based
filter as before (using the same threshold) and sort the data using this filter. How many variant sites
remain and how does this compare to filtering using the overall read depth?

We could in principle use any of the data fields present in the VCF file for further data filtering. But
let us assume we are satisfied with the quality of the remaining SNP calls.

We will now examine the sample genotype ( GT ) columns. The GT tag records the inferred sample
genotype at a variant position in a notation like 0/1 , where 0 is the reference allele ( REF ) and 1 the
first of the alternative ( ALT ) alleles. Split the GT tags by the forward slash (“ / ”) and label the resulting
columns by sample name and alleles 1 and 2.

Now, for each sample add a column indicating if it is heterozygous. This is the case if alleles 1 and 2
are different from each other: 

      =IF([Cell_Allele1]<>[Cell_Allele2];1;0)  
or 

      =IF([Cell_Allele1]<>[Cell_Allele2],1,0)

Here, the result 1 would indicate that samples are heterozygous. Given these values, what is the average observed
heterozygosity (H o ) over all variant sites for each of the samples? Are the samples similar or
different?

We have the alleles of four diploid samples and can hence calculate frequencies of the REF
(numbered 0) and ALT (numbered 1) alleles. If we call them p and q, we can further calculate HWE
expected heterozygosity as *__H e = 2pq__*. We can again average this value over all sites in the sample.
This allows us to compare expected and observed heterozygosity using the fixation index *__F = (H e -
H o )/H e__* . What does the result indicate? (Note that if we had a proper population sample, could now
go on to calculate F ST etc. directly from the data).

Would we expect a single gene to be heterozygous at some and homozygous at other sites? Have a
closer look at a few specific transcripts. What pattern do you see? What are the possible
explanations for this observation?

In several instances, we may want to know the bases occurring at a variant in a gene rather than
allele numbers. We can reconstruct the SNP calls at each site using a simple IF statement:

      =IF([Cell_Allele0]=0;[Cell_REF];[Cell_ALT])

or 

      =IF([Cell_Allele0]=0,[Cell_REF],[Cell_ALT])


Such a SNP table could then be used e.g. for input into other software.



