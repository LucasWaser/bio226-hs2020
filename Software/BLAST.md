# **_BLAST_**

Stands for **B**asic **L**ocal **A**lignment **S**earch **T**ool

BLAST finds regions of similarity between biological sequences. The program compares nucleotide or protein sequences to sequence databases and calculates the statistical significance.


BLAST can be used via a web inerface (see screenshot below). Have a look with this link: https://blast.ncbi.nlm.nih.gov/Blast.cgi

To download the latest Local Version visit: https://ftp.ncbi.nlm.nih.gov/blast/executables/LATEST/



![BLAST_Webinterface](Software/images/Screenshot_BLAST_WebBLAST.png)
