# __*SOFTWARE*__


>>>
### [BLAST](Software/BLAST.md)
BLAST finds regions of similarity between biological sequences. The program compares nucleotide or protein sequences to sequence databases and calculates the statistical significance.
>>>

>>>
### [Blast2GO](Software/Blast2GO.md)
Software with easy-to-use graphical user interface (GUI) that allows BLAST searches to a public database, and then extracts the Gene Ontology (GO) annotation of the BLAST hits.

>>>

>>>
### [gzip](Software/gzip.md)
gzip is a commandline tool for compressing and decompressing files
>>>

>>>
### [md5sum](Software/MD5_Checksum.md)
The MD5 message-digest algorithm is a cryptographically broken but still widely used hash function producing a 128-bit hash value.
>>>

>>>
### [Tablet assembly viewer](Software/TabletAV.md)
A lightweight viewer for next generation sequence assemblies and features, such as single nucleotide polymorphisms (SNPs).

>>>


