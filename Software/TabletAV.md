# __*Tablet Assembly Viewer*__

> Tablet is a lightweight, high-performance graphical viewer for next generation sequence assemblies and alignments.

<br>

Visit the [download](https://ics.hutton.ac.uk/tablet/download-tablet/) page to get the latest version of Tablet, available for *Windows Linux* & *macOS*.

![tablet1](images/tablet1.png)

For more information and detailed documentation visit the [Help Page](https://ics.hutton.ac.uk/tablet/download-tablet/)
