# *__Blast2GO__*

Is a Software with an easy-to-use graphical user interface (GUI) that allows BLAST searches to a public database, and then extracts the Gene Ontology (GO) annotation of the BLAST hits.

The installer can be downloaded from the official webpage https://www.biobam.com/blast2go-previous-versions/

![download_page_Blast2GO](images/Blast2GO_Download.png)







### Linux 

1. Download the [Linux 64bit (.sh)](https://www.biobam.com/blast2go-previous-versions/) installer from the official webpage 

![Screenshot_Download_Blast2GO_Unix](Screenshot_Download_Blast2GO_Unix.png)

2. Extract with right mouseclick (Extract here) or the command below.

        unzip Blast2GO_unix_6_0_3.zip
    
> *__Note__*: If the version has changed you might have to change the file name

3. Start the Setup Wizard with the following command.

        bash Blast2GO_unix_6_0_3.sh    

4. Follow the [Wizards instructions](#Wizards_instructions_Title).


### Windows 

1. Download the [Windows 64bit (.exe)](https://www.biobam.com/blast2go-previous-versions/) installer from the official webpage 

![Screenshot_Download_Blast2GO_Unix](/images/Screenshot_Download_Blast2GO_Unix.png)

2. Start the Setup Wizard by double clicking the Blast2GO_windows-x64_6_0_3(.exe , the extension might be hidden depending on system preferences)
![Screenshot_Blast2Go_file_Windows](images/Windows_Blast2GO_File_screenshot.png)


3. Follow the [Wizards instructions](#Wizards_instructions_Title).














## Setup Wizard <a name="Wizards_instructions_Title"></a>

1. Allow application to make changes to your device.

![Screenshot_Allow_changes](images/screenshot.png)

2. Follow the Wizards instructions.

![Screenshot_Welcome](images/Welcome_Setup_Wizard_Blast2GO_unix.png)



Once Blast2GO is open, it will ask for an [Activation Key](#Registering_Title). For this you will need to register on their webpage: https://www.blast2go.com/blast2go-pro/b2g-register-basic







## Registering Blast2GO <a name="Registering_Title"></a>


1. Access the blast2go webpage: https://www.blast2go.com/blast2go-pro/b2g-register-basic and fill in the form.
Please enter your **first and family name** and **e-mail**. At the “Institution” field, please write __University of
Zurich__ and, finally, country: **Switzerland**. Within a couple of minutes, you should get an e-mail with
the activation key. Please copy it in your Blast2GO window and it will start working.







