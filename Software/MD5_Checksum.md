# **_MD5-Checksum_**

The MD5 message-digest algorithm is a cryptographically broken but still widely used hash function producing a 128-bit hash value.

MD5 digests have been widely used in the software world to provide some assurance that a transferred file has arrived intact. For example, file servers often provide a pre-computed MD5 (known as md5sum) checksum for the files, so that a user can compare the checksum of the downloaded file to it. Most unix-based operating systems include MD5 sum utilities () in their distribution packages; Windows users may use the included PowerShell function "Get-FileHash".


### **_Ubuntu / macOS_**

The built in md5checksum tool is callable by typing md5sum ,below an example with a checksum file:

``` 
md5sum -c checkfile.md5 
```
If the checksome was created or altered on a Windows (dos) system, the linebreaks might have been replaced with /r/n instead of the Unix standard /n. In this case we can either check the sums by eye or use the code below to replaced the error causing portions of the file.

```
sed 's/\r$//' reads.md5 | md5sum -c
``` 




### **_Windows_**

Open a command prompt and enter the following:

```
CertUtil -hashfile <path to file> MD5
```
It is also possible to generate checksums for other hash algorithms by replacing the MD5 parameter but we won't be needing this (note that if you don’t specify a value then SHA1 is used by default)


