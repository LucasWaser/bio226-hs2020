# Introduction to next-generation sequence (NGS) data analysis


## Data
---
### Background information on data
NGS data files tend to be huge, typically many Gb in size. Consequently it can be tricky to manage
these data files and their analysis can take a considerable amount of computing time. In order to
simplify this computer lab, we will therefore only work with a tiny subset of a real data set.
The data to be analysed here are from an mRNA-Seq experiment that was performed using the
Illumina HiSeq 2000 platform. You are given 4 FASTQ files containing (a subset of) reads from 4
different plant species, together with a (sub)set of reference contigs produced by transcriptome de
novo assembly.

### Data files
Reference transcript sequences (264 reference sequences) are in FASTA format in the file
selected_genes_A.fas . (If you want to view them, you can open this file in BioEdit .)
Read data are in GZIP-compressed FASTQ format files, using Sanger (Phred+33) quality encoding:
Species E: E.fq.gz containing 140590 reads.
Species G: G.fq.gz containing 197583 reads.
Species S: S.fq.gz containing 423579 reads.
Species I: I.fq.gz containing 195365 reads.
Each read is 101 nucleotides long.

The MD5 check-sum file reads.md5 (feel free to view it in a text editor) contains check-sums for
*E.fq , G.fq , S.fq* and *I.fq* , i.e. the uncompressed FASTQ files.

**Copy** the data to a working directory within your Workshop folder.

---
<br>

## Software
---
In this computer session, we will use the command line, [gzip] , [md5sum] , [FastQC] , [bowtie2] and/or
[bwa] , [Tablet assembly viewer](Software/TabletAV.md) (genome browser), Excel and possibly FAMD . For the bwa and
bowtie2 read mappers and command-line utilities, we provide you with Windows versions (which
are not necessarily of the most recent software version) and also more up-to-date Linux versions that
should work under Linux, including on bash under the Windows Linux Subsystem (WSL). You are free
to use either Windows (the default option in this course) or WSL/Linux for this exercise, depending
on your preferences and level of experience. The Windows executables are suffixed .exe , the Linux
ones are without suffix.

