# Introduction to remote work on a server

### Table of contents

- [1 Prepare your computer](#part1)
   - [1.1 Windows: Use PuTTY](#part1_1)
   - [1.2 MacOS: Use the terminal](#part1_2)
- [2 Connect remotely to our server](#part2)
  - [2.1 PuTTY](#part2_1)
  - [2.2 MacOS terminal](#part2_2)
- [3 Get familiar with the command line interface](#part3)
- [4 Move files between the server and your PC or Mac](#part4)


Further information:

  - [How to solve problems in a command line environment](#exc2)

#### Aims of this introduction:

- getting familiar with the command-line environment
- learn how to remotely connect to a server and run programs on the server

## 1 Prepare your computer <a name="part1"></a>

### 1.1 Windows: Use PuTTY <a name="part1_1"></a>

1. Download PuTTY from https://www.puttygen.com/download-putty (you have to scroll down a bit)

![putty1](images/putty1.png)

2. open the installer file and install putty (the default settings should be fine)

3. Open PuTTYgen

![putty2](images/putty2.png)

4. Click "Generate" to generate a new key pair

![putty3](images/putty3.png)

5. Write "bio226 course" and your name into the Key comment field and choose a password [Blue box]; Save the private key somewhere on your PC, you can give the file any name but it should have the ppk ending e.g. bio226.ppk <a name="poi1"></a> [Red arrow]; Copy the public part of your key [Red box] and send a mail to me (alexander.kirbis@uzh.ch) with the content of the red box, so I can add your authentification to the server

![putty4](images/putty4.png)

Note that you can always re-generate your public key by opening your saved private key with the **Load** button.

6. Now you can close PuTTYgen and continue with [section 2.1](#part2_1)

### 1.2 MacOS: Use the terminal <a name="part1_2"></a>

1. Open the terminal by going to Applications > Utilities > Terminal

2. In the terminal type the following command

```
ssh-keygen -t rsa
```

3. You will be asked specify a file in which to save the key, or just press Enter to choose the default location (/home/yourusername/.ssh/id_rsa)

```
Enter file in which to save the key (/home/youruser/.ssh/id_rsa):
```

4. Then you will be prompted to choose a password (you can also leave this empty, but it's not recommended) Remember the password, as you will have to use it later.

```
Enter passphrase (empty for no passphrase):
```

Once you confirm your password, two key files will be created in the directory you specified in 3. One has the .pub ending and can be shared with others. The key file without .pub is your private key and should not be shared.

5. Now you have to add your private key to the ssh manager of your system with the command (replace the path with the path set in 3.):

```
ssh-add /home/youruser/.ssh/id_rsa
```

6. Finally, I have to add your public key to the server. To display your key you can use the following command:

```
cat /home/youruser/.ssh/id_rsa.pub
```

Send the public key via e-mail to Lucas.Waser@uzh.ch. You can either copy the content of the file, or send me the .pub file. Once I have added your key to the server, you can continue with 2.2.

## 2. Connect remotely to our server <a name="part2"></a>

You have to be connected to the UZH network before attempting to connect to our server, just start up your VPN connection. If you need more information: https://www.zi.uzh.ch/en/support/netzwerk/vpn.html

### 2.1 PuTTY <a name="part2_1"></a>

6. Open PuTTY, go to the __Session__ tab, and enter the Host IP adress shown in the image. Then go to the **Auth** tab and enter the path to the key file you created earlier (e.g. bio226.ppk)

![putty5](images/putty5.png)

7. You can save your session settings by going back to **Session**, entering a Session name and hitting the **Save** button. If you don't save a session profile you have to repeat step 6 every time you restart PuTTY

![putty6](images/putty6.png)

8. Now press the **Open** button to establish a connection to the server. A new window will open where you have to enter the login name "ubuntu". Then you will be asked for the password you set in [section 1.1](#poi1) (the password will not show up as you type it). Finally hit enter and you should be connected to the server

![putty7](images/putty7.png)

### 2.2 MacOS terminal <a name="part2_2"></a>

1. Open the terminal (see 2.1)

2. Now use the following command to connect to our server

```
ssh ubuntu@172.23.40.67
```

You will be asked for the password you set in 2.1. When your connection was successful, you should see something like this:

![mac_terminal](images/mac_terminal.png)

If you were able to establish the connection you can continue with 3 and 4.

## 3. Get familiar with the command line interface <a name="part3"></a>

Numerous introductions to the UNIX command line interface are already written, therefore I am not providing you a detailed tutorial here but encourage you to search for existing tutorials that fit your experience level. Here's a link to the official Ubuntu command-line tutorial from Canonical, which is rather detailed:

https://ubuntu.com/tutorials/command-line-for-beginners#1-overview


Here's a useful cheat-sheet with the most important commands (source: https://www.slideshare.net/NoFernndezPozo/unix-command-sheet2014):

![cheat_sheet](images/cmd_cheat_sheet.jpg)

##### Running processes in the background and with nohup

When you start a program in the terminal, you will not be able to do anything else until it has finished execution. This is usually no problem as most commands finish almost immediately. However, sometimes you will run bigger analyses that take a while to finish and you might want to continue working in the terminal. To do so, you can start a program in the background by appending `&`:

```
your_command &
```

If you have already started the command and want to put it in the background, use the key combination `Ctrl + Z` to stop the process and then use the command `bg` to resume it in the background. You can display all running tasks with the command `jobs`

Each process or task you start in your terminal is by default terminated if you close the connection. However, some analyses take hours or days to finish, so we need a way to run programs independent from our connection to the server. To achieve this you can use the command `nohup`

```
nohup your_command &
```

Note the `&` at the end of the line. This puts your command in the background, so you can continue working in the terminal while the program is running. You can display all running tasks and see their status with the command:

```
jobs -l
```

By default the standard output and error messages are saved in you current working directory (you can see your current working directory with the command `pwd`) in a file named nohup.out. However, this file will be overwritten each time you start a new nohup command, so it is a good idea to redirect the ouput to its own log file:

```
nohup your_command > ~/storage/users/your_name/command.log &
```

You can specify any filename, but I recommend that you save your log files in your personal directory on the server to avoid confusion.

---
**How to solve problems in a command line environment** <a name="exc2"></a>

When working remotely on a server with the command line interface, there might come up a variety of problems that is impossible to cover within the scope of this tutorial. Whenever you encounter errors or other problems, it is important that you ask yourself a couple of questions:

1. What is the actual Problem? Is a program not working properly or is there some problem with the input files? Is the error message giving me any hint?

2. Is the code I used to execute the program correct?

   - Remember that many commands are case sensitive. It is very important that you don't have a spelling error in your command line input. If you have to provide paths to specific files, make sure that you use the auto-completion function of the command line terminal (press \<TAB> to complete the file or directory name you started typing)

If you are not able to solve the problem immediately, a short search with the search engine of your trust, using e.g. the error message you got, can sometimes provide quick help. Otherwise, don't hesitate to contact one of the supervisors for some help.

---

## 4. Move files between the server and your PC <a name="part4"></a>

1. Download Filezilla from https://filezilla-project.org/download.php?type=client. Choose the appropriate client for your system.

![filezilla1](images/filezilla1.png)

2. Select the marked download button and download the installation file

![filezilla2](images/filezilla2.png)

3. Execute the installation file and start the installation, the standard parameters are usually fine, **BUT**

![filezilla3](images/filezilla3.png)

**BE CAREFUL NOT TO INSTALL THE BLOATWARE THAT COMES WITH THE INSTALLATION!**

![filezilla4](images/filezilla4.png)

4. After the installation you can start FileZilla. First go to the **Edit** tab and open **Settings**. In the Settings window go to the **SFTP** tab and click the button "add keyfile". Browse to the keyfile you created in [Section 1.2](#poi1) and add it to the list.

![filezilla5](images/filezilla5.png)

5. Enter the details in the main window as shown. The password is the password you set in [Section 1.2](#poi1). Then hit **Connect** to connect to the server.

![filezilla6](images/filezilla6.png)

6. On the left side you see the files on your system, while the files on the server appear on the right side. Please do not change anything in the home directory. All files for the course should be handled inside the **storage** directory (red arrow)

![filezilla7](images/filezilla7.png)

7. Inside the storage directory you find the material for the **student_projects**, **software** you may need for the course and this **README.md**. The first thing you can do, is to create your own directory in which you will work during the course as Manuel kindly demonstrated in the image below. Now you can move files back and forth like in a normal explorer.

![filezilla8](images/filezilla8.png)
